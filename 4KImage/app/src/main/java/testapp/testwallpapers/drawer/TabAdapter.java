package testapp.testwallpapers.drawer;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import java.util.List;

import testapp.testwallpapers.MainActivity;

//класс  заполняtn вкладки, отображая на них соответствующие фрагменты.
public class TabAdapter extends FragmentStatePagerAdapter {

    List<NavItem> actions;
    Context context;
    private Fragment mCurrentFragment;

    //В конструкторе список пунктов и контекст.
    public TabAdapter(FragmentManager fm, List<NavItem> action, Context context) {
        super(fm);
        this.actions = action;
        this.context = context;
    }


    //В методе getItem возвращаем фрагмент для соответствующей позиции,
    // вызывая метод fragmentFromAction с передачей ему пункта списка панели вкладок.
    @Override
    public Fragment getItem(int position)
    {
        Fragment fragment = fragmentFromAction(actions.get(position));
        return fragment;
    }

    //Метод  удаляет страницу для заданной позиции.
    @Override
    public void destroyItem (ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }

    //Метод  возвращает количество пунктов навигации.
    @Override
    public int getCount() {
        return actions.size();
    }

    //Метод вызывается для информирования адаптера о том,
    // какой элемент в настоящее время считается "основным", т. е. отображается в качестве текущей страницы для пользователя.
    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (getCurrentFragment() != object) {
            mCurrentFragment = ((Fragment) object);
        }
        super.setPrimaryItem(container, position, object);
    }


    public Fragment getCurrentFragment() {
        return mCurrentFragment;
    }

    ////Метод возвращает заголовок вкладки в соответствии с позицией.
    @Override
    public CharSequence getPageTitle(int position) {
        return actions.get(position).getText(context);
    }

    public static Fragment fragmentFromAction(NavItem action){
        try {
            Fragment fragment = action.getFragment().newInstance();

            Bundle args = new Bundle();
            args.putStringArray(MainActivity.FRAGMENT_DATA, action.getData());

            fragment.setArguments(args);

            return fragment;
        } catch (InstantiationException e) {

        } catch (IllegalAccessException e) {

        }

        return null;
    }
}

