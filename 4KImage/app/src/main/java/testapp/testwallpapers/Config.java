package testapp.testwallpapers;


import testapp.testwallpapers.drawer.SimpleMenu;

public class Config {


    public static String CONFIG_URL = "";


    public static final boolean HIDE_DRAWER = false;


    public static boolean USE_HARDCODED_CONFIG = false;

    //константа для указания интервала отображения межстраничного объявления
    public static final int INTERSTITIAL_INTERVAL = 5;


    public static void configureMenu(SimpleMenu menu, ConfigParser.CallBack callback){


        callback.configLoaded(false);
    }

}