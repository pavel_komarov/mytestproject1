package testapp.testwallpapers.inherit;

//PermissionsFragment для запроса разрешений для фрагментов,
// возвращает Массив всех разрешений, необходимых для этого фрагмента.

public interface PermissionsFragment {

    String [] requiredPermissions();
}
