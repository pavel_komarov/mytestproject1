package testapp.testwallpapers.inherit;


// интерфейс BackPressFragment для обработки нажатия кнопки «назад» во фрагментах.
//Возвращает true, если backpress был обработан или false в противном случае.
public interface BackPressFragment {

    boolean handleBackPress();
}
