package com.pavelkomarof.cryptomarket.chart

import android.widget.TextView
import android.annotation.SuppressLint
import android.content.Context
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import com.pavelkomarof.cryptomarket.R
import com.pavelkomarof.dateToString


//Класс наследуется от библиотечного класса MarkerView. Конструктор класса принимает контекст и ссылку на макет маркера.
// Далее объявляем и инициализируем текстовое поле из макета.
@SuppressLint("ViewConstructor")
class MyMarkerView(context: Context, layoutResource: Int) : MarkerView(context, layoutResource) {

    private val tvContent: TextView

    init {

        tvContent = findViewById(R.id.tvContent)
    }


    //  refreshContent принимает объект класса Entry — это запись на графике, может содержать одно или множество значений.
    // Второй параметр — Highlight — содержит информацию, необходимую для определения выделенного значения.
    override fun refreshContent(e: Entry, highlight: Highlight) {


        //заполняем текстовое поле маркера значениями графика по осям с применением необходимого форматирования
        tvContent.text = e.y.toString() + "\n" + e.x.dateToString("MMM dd, yyyy")


        super.refreshContent(e, highlight)
    }

    //Функция getOffset() возвращает положение маркера.
    override fun getOffset(): MPPointF {
        return MPPointF((-(width / 2)).toFloat(), (-height).toFloat())
    }
}