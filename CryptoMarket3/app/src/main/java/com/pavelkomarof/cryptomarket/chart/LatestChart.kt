package com.pavelkomarof.cryptomarket.chart

import android.content.Context
import android.graphics.Color
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.pavelkomarof.cryptomarket.R
import com.pavelkomarof.cryptomarket.di.App
import com.pavelkomarof.cryptomarket.formatters.YearValueFormatter

import javax.inject.Inject

class LatestChart {

    //Инжектим контекст и форматтер, объявляем объект класса LineChar для построения графика
    @Inject
    lateinit var context: Context

    @Inject
    lateinit var formatter: YearValueFormatter

    lateinit var chart: LineChart


    //Инициализируем AppComponent Даггера.
    init {
        App.appComponent.inject(this)
    }

    //Функция initChart инициализирует график. можно изменять такие параметры графика, как описание,
    // поддержку жестов, масштабирование и перетаскивание, причем масштабирование может выполняться только по одной из осей.
    fun initChart(chart: LineChart) {
        this.chart = chart



        chart.description.isEnabled = false


        chart.setTouchEnabled(true)


        chart.isDragEnabled = true
        chart.setScaleEnabled(false)
        chart.isScaleXEnabled = true
        chart.setDrawGridBackground(false)
        chart.isDoubleTapToZoomEnabled = false


        chart.setPinchZoom(false)


        chart.maxHighlightDistance = 300F

        //Далее объявляем объект класса LineData, который инкапсулирует все данные для графика
        val data = LineData()
        //цвет отрисовки данных
        data.setValueTextColor(Color.BLACK)

        //отдаем объект графику
        chart.data = data


        chart.legend.isEnabled = true


        //setDrawMarkers включает опцию отрисовки маркера, инициализирует маркер.
        chart.setDrawMarkers(true)
        chart.marker =
            MyMarkerView(context, R.layout.custom_marker_view)

        // параметры отображения осей, надписей, цвета текста
        val xl = chart.xAxis
        xl.textColor = Color.BLACK
        xl.position = XAxis.XAxisPosition.BOTTOM
        xl.setDrawGridLines(false)
        xl.valueFormatter = formatter
        xl.labelCount = 3
        xl.granularity = 48F


        xl.setAvoidFirstLastClipping(true)
        xl.isEnabled = true

        val leftAxis = chart.axisLeft
        leftAxis.textColor = Color.BLACK
        leftAxis.setDrawGridLines(true)

        val rightAxis = chart.axisRight
        rightAxis.isEnabled = true

    }

    //добавление данных на график используя объект интерфейса ILineDataSet
    fun addEntry(value: Float, date: Float) {
        val data = chart.data

        if (data != null) {

            var set: ILineDataSet? = data.getDataSetByIndex(0)

            if (set == null) {
                set = createSet()
                data.addDataSet(set)
            }


            data.addEntry(Entry(date, value), 0)
            data.notifyDataChanged()


            //для уведомления об изменении данных
            chart.notifyDataSetChanged()

            //смещает окно графика в нужную позицию по оси x и инициирует обновление графика
            chart.moveViewToX(date)

            //Выделяет значение y при данном значении x в данном наборе данных.
            chart.highlightValue(date, 0)
        }
    }

    //создание и настройка набора данных
    private fun createSet(): LineDataSet {
        val set = LineDataSet(null, "Price, USD")

        set.mode = LineDataSet.Mode.CUBIC_BEZIER
        set.cubicIntensity = 0.2f
        set.setDrawFilled(true)
        set.setDrawCircles(false)
        set.lineWidth = 1.8f
        set.circleRadius = 4f
        set.setCircleColor(Color.BLACK)
        set.highlightLineWidth = 1.2f
        set.highLightColor = context.resources.getColor(R.color.colorAccent)
        set.color = Color.BLACK
        set.fillColor = Color.BLACK
        set.enableDashedHighlightLine(10f, 5f, 0f)
        set.fillAlpha = 100
        set.setDrawHorizontalHighlightIndicator(true)
        set.setFillFormatter { _, _ ->
            chart.axisLeft.axisMinimum
        }
        return set
    }

}
