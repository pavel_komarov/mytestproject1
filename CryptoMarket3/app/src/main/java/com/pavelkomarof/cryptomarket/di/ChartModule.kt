package com.pavelkomarof.cryptomarket.di

import com.pavelkomarof.cryptomarket.chart.LatestChart
import com.pavelkomarof.cryptomarket.formatters.YearValueFormatter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

//Модуль ChartModule будет использоваться для работы с графиком
@Module
class ChartModule {
    @Provides
    @Singleton
    fun provideLatestChart() = LatestChart()

    @Provides
    @Singleton
    fun provideYearFormatter() = YearValueFormatter()
}