package com.pavelkomarof.cryptomarket.di

import com.pavelkomarof.cryptomarket.activities.ChartActivity
import com.pavelkomarof.cryptomarket.activities.MainActivity
import com.pavelkomarof.cryptomarket.chart.LatestChart
import com.pavelkomarof.cryptomarket.fragments.CurrenciesListFragment
import com.pavelkomarof.mvp.presenter.CurrenciesPresenter
import com.pavelkomarof.mvp.presenter.LatestChartPresenter
import dagger.Component

import javax.inject.Singleton
//Данной аннотацией говорим Даггеру, что AppComponent содержит четыре модуля: AppModule, ChartModule, MvpModule и RestModule.
//Зависимости, которые провайдит каждый из этих модулей, доступны для всех остальных модулей, объединенных в компоненте AppComponent.
@Component(modules = arrayOf(AppModule::class, RestModule::class, MvpModule::class, ChartModule::class))
@Singleton
interface AppComponent {

    //функция inject(mainActivity: MainActivity), которая сообщает Даггеру класс, в который мы хотим внедрять зависимости
    fun inject(mainActivity: MainActivity)

    fun inject(fragment: CurrenciesListFragment)

    fun inject(presenter: CurrenciesPresenter)
    fun inject(presenter: LatestChartPresenter)
    fun inject(chart: LatestChart)
    fun inject(activity: ChartActivity)

}
