package com.pavelkomarof.cryptomarket.activities


import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.pavelkomarof.cryptomarket.R
import com.pavelkomarof.cryptomarket.chart.LatestChart
import com.pavelkomarof.cryptomarket.di.App
import com.pavelkomarof.mvp.contract.LatestChartContract
import com.pavelkomarof.mvp.presenter.LatestChartPresenter

import kotlinx.android.synthetic.main.activity_chart.*
import java.text.DecimalFormat
import javax.inject.Inject

//Имплементируем интерфейсы OnChartValueSelectedListener, LatestChartContract.View библиотеки для графика, реализуем их методы.
class ChartActivity : AppCompatActivity(), OnChartValueSelectedListener, LatestChartContract.View {


    @Inject
    lateinit var latestChart: LatestChart

    @Inject
    lateinit var presenter: LatestChartPresenter


    // в onCreate отдаем презентеру ссылку на текущее активити, и создаем переменные со значениями, полученными из интента
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chart)

        App.appComponent.inject(this)
        presenter.attach(this)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val name = intent.getStringExtra("name")
        val marketCapRank = intent.getIntExtra("marketCapRank", 0)
        val symbol = intent.getStringExtra("symbol")
        val marketCap = intent.getStringExtra("marketCap")
        val marketCapChangePercentage24h = intent?.getFloatExtra("marketCapChangePercentage24h", 0.0f)
        val priceChangePercentage24h = intent?.getFloatExtra("priceChangePercentage24h", 0.0f)
        val totalVolume = intent?.getFloatExtra("totalVolume", 0.0f)
        val ath = intent?.getFloatExtra("ath", 0.0f)
        val athChangePercentage = intent?.getFloatExtra("athChangePercentage", 0.0f)
        val circulatingSupply = intent?.getDoubleExtra("circulatingSupply", 0.0)
        val totalSupply = intent?.getLongExtra("totalSupply", 0)
        val image = intent.getStringExtra("image")

        // При помощи библиотеки Glide загружаем изображение иконки криптовалюты
        Glide.with(this).load(image).into(ivCurrencyDetailIcon)

        supportActionBar?.title = name

        val df = DecimalFormat("#")
        df.maximumFractionDigits = 2

        //Заполняем поля макета экрана, применяя форматирование
        tvDetailMarketCapRank.text = marketCapRank.toString()
        tvMarketCapChange.text = marketCapChangePercentage24h.toString()
        tvATH.text = ath.toString()
        tvAthChange.text = df.format(athChangePercentage)
        tvCirculatingSupply.text = df.format(circulatingSupply)
        tvTotalSupply.text = totalSupply.toString()



        // Затем через презентер создаем график и инициализируем его.
        presenter.makeChart(intent.getStringExtra("id"))

        latestChart.initChart(chartCurrency)
    }

    override fun onNothingSelected() {
        TODO("not implemented")
    }

    override fun onValueSelected(e: Entry?, h: Highlight?) {
        TODO("not implemented")
    }

    // addEntryToChart() для добавления данных на график
    override fun addEntryToChart(date: Float, value: Float) {

        latestChart.addEntry(value, date)
    }

    override fun addEntryToChart(value: Float, date: String) {
        TODO("not implemented")
    }

    //showProgress() и hideProgress() для отображения и скрытия прогрессбара
    override fun showProgress() {
        progressChart.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressChart.visibility = View.INVISIBLE
    }

    // showErrorMessage() для отображения ошибки
    override fun showErrorMessage(error: String?) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    override fun refresh() {
        TODO("not implemented")
    }


    override fun onResume() {
        super.onResume()
        presenter.attach(this)
    }

    //Чтобы презентер не пересоздавался вместе с активити при повороте экрана, отсоединяем его в onPause() и присоединяем в onResume().
    override fun onPause() {
        super.onPause()
        presenter.detach()
    }

}