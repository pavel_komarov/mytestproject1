package com.pavelkomarof.cryptomarket.di

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

//Класс предоставляет контекст для доступа к ресурсам в любой части приложения
@Module
class AppModule(private val app: App) {

    @Provides
    @Singleton
    fun provideContext(): Context = app

}

//Класс помечен аннотацией @Module, сообщающей Даггеру, что функции этого класса предоставляют зависимости.
// Функция provideContext() помечена аннотацией @Provides как раз для этой цели.
//
//Аннотация @Singletone означает, что Даггер при инициализации компонента создаст единственный экземпляр помеченной зависимости, то есть синглтон.
// И при каждом затребовании данной зависимости будет предоставлять этот единственный экземпляр.
// Это позволит избежать ненужного пересоздания объектов, утечек памяти и других проблем.