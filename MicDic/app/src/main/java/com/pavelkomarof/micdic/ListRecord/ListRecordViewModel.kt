package com.pavelkomarof.micdic.ListRecord

import androidx.lifecycle.ViewModel
import info.fandroid.voicerecorder.database.RecordDatabaseDao

//ViewModel обеспечит  сохранение данных при повороте экрана и других изменениях конфигурации
class ListRecordViewModel(
    dataSource: RecordDatabaseDao
) : ViewModel() {

    ////Инициализирую базу данных и получаю все записи, которые нужно будет отобразить в списке
    val database = dataSource
    val records = database.getAllRecords()

}