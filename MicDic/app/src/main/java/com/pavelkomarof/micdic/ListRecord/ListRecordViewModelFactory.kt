package com.pavelkomarof.micdic.ListRecord

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import info.fandroid.voicerecorder.database.RecordDatabaseDao


//класс для создания объекта ViewModel с параметрами
// конструктора и возврата его экземпляра, выжившего после изменения конфигурации.
class ListRecordViewModelFactory(
    private val dataSource: RecordDatabaseDao
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ListRecordViewModel::class.java)) {
            return ListRecordViewModel(dataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}