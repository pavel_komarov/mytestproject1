package info.fandroid.voicerecorder.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
//@Entity, в скобках которой  параметр tableName с именем таблицы, которая будет хранить эти сущности.
@Entity(tableName = "recording_table")
data class RecordingItem(
    //@PrimaryKey –  поле идентификатора, который будет ключом
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L,
    @ColumnInfo(name = "name")
    var name: String = "",
    @ColumnInfo(name = "filePath")
    var filePath: String = "",
    @ColumnInfo(name = "length")
    var length: Long = 0L,
    @ColumnInfo(name = "time")
    var time: Long = 0L
)
//имя, путь к файлу записи, длина звукового файла и время его создания. Все эти поля обозначаю аннотациями @ColumnInfo,
// которые обеспечат создание в таблице соответствующих столбцов. Параметр name передает заголовок столбца.