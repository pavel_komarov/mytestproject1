package info.fandroid.voicerecorder.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface RecordDatabaseDao {
    //добавление записей в базу данных
    @Insert
    fun insert(record: RecordingItem)

    @Update
    fun update(record: RecordingItem)

    // @Query выполняют выборки данных из БД и операции над ними
    @Query("SELECT * from recording_table WHERE id = :key")
    //getRecord выполняет поиск определенных записей по ключу — идентификатору
    fun getRecord(key: Long?): RecordingItem?

    @Query("DELETE FROM recording_table")
    //clearAll удаляет все записи из БД
    fun clearAll()

    @Query("DELETE FROM recording_table WHERE id = :key")
    //removeRecord удаляет одну запись по идентификатору
    fun removeRecord(key: Long?)

    @Query("SELECT * FROM recording_table ORDER BY id DESC")
    //getAllrecords выбирает все записи и сохраняет их в виде экземпляра класса LiveData, содержащего список записей
    fun getAllRecords(): LiveData<MutableList<RecordingItem>>

    @Query("SELECT COUNT(*) FROM recording_table")
    //getCount возвращает количество записей в БД
    fun getCount(): Int
}