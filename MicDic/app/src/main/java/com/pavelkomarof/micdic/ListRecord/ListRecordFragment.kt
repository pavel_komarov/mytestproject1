package info.fandroid.voicerecorder.listRecord

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil

import androidx.fragment.app.Fragment

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.pavelkomarof.micdic.ListRecord.ListRecordAdapter
import com.pavelkomarof.micdic.ListRecord.ListRecordViewModel
import com.pavelkomarof.micdic.ListRecord.ListRecordViewModelFactory
import com.pavelkomarof.micdic.R
import com.pavelkomarof.micdic.databinding.FragmentListRecordBinding
import info.fandroid.voicerecorder.database.RecordDatabase


class ListRecordFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: FragmentListRecordBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_list_record, container, false)

        val application = requireNotNull(this.activity).application


        //Получаю экземпляр базы данных.
        val dataSource = RecordDatabase.getInstance(application).recordDatabaseDao
        val viewModelFactory = ListRecordViewModelFactory(dataSource)


        //Получаю экземпляр ListRecordViewModelFactory для создания и работы с экземплярами ViewModel.
        val listRecordViewModel =
            ViewModelProviders.of(
                this, viewModelFactory).get(ListRecordViewModel::class.java)


        binding.listRecordViewModel = ListRecordFragment()

        //Получаю адаптер списка и привязываю его к DataBinding.
        val adapter =
            ListRecordAdapter()
        //подписываю адаптер на изменения списка записей.
        binding.recyclerView.adapter = adapter


        //указываю текущий фрагмент в качестве владельца жизненного цикла,
        // это нужно для привязки DataBinding к обновлениям LiveData.
        listRecordViewModel.records.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.data = it
            }
        })


        binding.lifecycleOwner = this

        //Возвращаю binding.root, который возвращает привязанный к DataBinding  файл макета.
        return binding.root
    }

}