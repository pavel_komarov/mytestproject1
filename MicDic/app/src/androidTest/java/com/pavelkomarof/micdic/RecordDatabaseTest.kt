package com.pavelkomarof.micdic

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import info.fandroid.voicerecorder.database.RecordDatabase
import info.fandroid.voicerecorder.database.RecordDatabaseDao
import info.fandroid.voicerecorder.database.RecordingItem
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import java.lang.Exception


@RunWith(AndroidJUnit4::class)
class RecordDatabaseTest {

    private lateinit var recordDatabaseDao: RecordDatabaseDao
    private lateinit var database: RecordDatabase

    //@Before, обеспечивающей необходимую подготовку перед тестами
    @Before
    fun createDb() {
        //Предоставление контекста для всех операций обеспечивает класс InstrumentationRegistry
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        database = Room.inMemoryDatabaseBuilder(context, RecordDatabase::class.java)
                //allowMainThreadQueries, позволяет обращаться к базе данных в основном потоке,
            // что следует делать только во время тестирования.
            .allowMainThreadQueries()
            .build()

        recordDatabaseDao = database.recordDatabaseDao
    }

    //@After выполняется для закрытия базы данных.
    @After
    @Throws(IOException::class)
    fun closeDb() {
        database.close()
    }

    //@Test,  вставляю запись в базу данных при помощи функции insert()
    // и получаю количество записей при помощи функции getCount().
    @Test
    @Throws(Exception::class)
    fun testDatabase() {
        recordDatabaseDao.insert(RecordingItem())
        val getCount = recordDatabaseDao.getCount()
        //assertEquals сравнивает значение функции getCount() с контрольным, равным единице,
        // поскольку у нас будет создана только одна запись в БД
        assertEquals(getCount, 1)
    }
}