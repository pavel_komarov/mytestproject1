package komarov.pavel.planner.dialog;

import android.content.DialogInterface;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;

import komarov.pavel.planner.Utils;

public class MyTimePickerFragment extends TimePickFragment {

    public EditText etTime;
    public Calendar calendar;

    @Override
    public void onTimeSet (TimePicker view, int hourOfDay, int minute) {
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);
        etTime.setText(Utils.getTime(calendar.getTimeInMillis()));
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        etTime.setText(null);
    }
}
