package komarov.pavel.planner.dialog;

import android.content.DialogInterface;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

import komarov.pavel.planner.Utils;

public class MyDatePickerFragment extends DatePickerFragment{

    public EditText  etDate;
    public Calendar calendar;


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        etDate.setText(Utils.getDate(calendar.getTimeInMillis()));

    }

    @Override
    public void onCancel (DialogInterface dialog) {
        etDate.setText(null);
    }

}
