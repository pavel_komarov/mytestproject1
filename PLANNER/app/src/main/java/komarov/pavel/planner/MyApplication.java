package komarov.pavel.planner;

import android.app.Application;

public class MyApplication extends Application {

    private static boolean activityVisible;

    public static boolean isActivityVisisble() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }
}
